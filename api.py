# coding=utf-8

from flask import abort, Flask, request, Response, send_file
from pymongo import MongoClient

app = Flask(__name__)
common_languages = ['fr-FR', 'en-US']


def rainfall_to_range(rainfall, lang, localized_strings):
    if rainfall == 0:
        return localized_strings['no_precipitations'][lang]
    elif rainfall >= 1 and rainfall <= 30:
        return localized_strings['little_rainfall'][lang]
    elif rainfall >= 31 and rainfall <= 50:
        return localized_strings['average_rainfall'][lang]
    elif rainfall >= 51 and rainfall <= 100:
        return localized_strings['heavy_rainfall'][lang]
    elif rainfall > 100:
        return localized_strings['very_heavy_rainfall'][lang]


def wind_speed_to_range(windspeed, lang, localized_strings):
    if windspeed >= 0 and windspeed <= 20:
        return localized_strings['light_wind'][lang]
    elif windspeed >= 21 and windspeed <= 50:
        return localized_strings['average_wind'][lang]
    elif windspeed >= 50 and windspeed <= 88:
        return localized_strings['strong_wind'][lang]
    elif windspeed > 88:
        return localized_strings['storm'][lang]


def degree_to_direction(degrees, lang, localized_strings):
    if (degrees > 0 and degrees < 45) or (degrees >= 315 and degrees <= 360):
        return localized_strings['north'][lang]
    elif degrees >= 45 and degrees < 135:
        return localized_strings['east'][lang]
    elif degrees >=135 and degrees < 225:
        return localized_strings['south'][lang]
    elif degrees >= 225 and degrees < 315:
        return localized_strings['west'][lang]


def inject_language(string, lang):
    global common_languages

    if lang in common_languages:
        return string.replace("{{language}}", lang)

    return string.replace('{{language}}', common_languages[0])


def dtmf_for_region(index):
    if index < 10:
        return "0" + str(index)
    return str(index)


def choose_region(lang):
    district_menu = u'<menu id="{{region_name}}">' \
                    u'<property name="inputmodes" value="dtmf"/>' \
                    u'<prompt>{{select_region}}</prompt>' \
                    u'{{choices}}' \
                    u'</menu>'
    district_choice = u'<choice dtmf="{{region_code}}" next="#{{region_name}}" />'
    xml = u'<vxml version="2.1" xml:lang="{{language}}">' \
          u'<menu id="regions">' \
          u'<property name="inputmodes" value="dtmf"/>' \
          u'<prompt>{{select_region}}</prompt>' \
          u'{{choices}}' \
          u'</menu>' \
          u'{{district_menus}}' \
          u'</vxml>'
    choice_template = u'<choice dtmf="{{region_code}}" ' \
                      u'next="/api/weather/region?lang={{language}}&amp;reg={{region_code}}" />'
    client = MongoClient('localhost', 27017)  # connect to MongoDB running on port 27017
    db = client['ict4d']  # get database 'ict4d'
    weather_collection = db['weather']  # get collection 'weather'
    regions = db['regions'].find_one({})['regions']
    weather_data = weather_collection.find({})  # get all documents
    region_choices = ''
    region_prompts = ''
    district_menus = ''
    localized_strings = db['strings'].find_one({})['strings']
    j = 1
    print(regions)
    for key in regions.keys():
        code = str(j)
        region_choice = district_choice.replace("{{region_code}}", code)
        region_choice = region_choice.replace("{{region_name}}", key.replace(' ', ''))
        region_choices += region_choice
        prompt = localized_strings['choose_region'][lang].replace("{{region_name}}", key)
        prompt = prompt.replace("{{region_code}}", code)
        region_prompts += prompt
        prompts = ''
        choices = ''
        menu = district_menu.replace("{{region_name}}", key.replace(' ', ''))

        for i in range(1, weather_collection.count() + 1):
            if weather_data[i-1]['name'] in regions[key]:
                code = dtmf_for_region(i)
                prompt = localized_strings['choose_region'][lang].replace("{{region_name}}", weather_data[i-1]['name'])
                prompt = prompt.replace("{{region_code}}", code)
                prompts += prompt
                choice = choice_template.replace("{{region_code}}", code)
                choice = choice.replace("{{language}}", lang)
                choices += choice

        menu = menu.replace("{{select_region}}", prompts)
        menu = menu.replace("{{choices}}", choices)
        district_menus += menu
        j = j + 1

    result = inject_language(xml, lang)
    result = result.replace("{{select_region}}", region_prompts)
    result = result.replace("{{choices}}", region_choices)
    result = result.replace("{{district_menus}}", district_menus)

    return Response(result, mimetype="text/xml")


def choose_language():
    xml = u'<vxml version="2.1">' \
          u'<audio src="/jingle"/>' \
          u'<menu id="lang">' \
          u'<property name="inputmodes" value="dtmf"/>' \
          u'<prompt xml:lang="en-US">For English, press one.</prompt>' \
          u'<prompt xml:lang="fr-FR">Pour la langue français, tapez 2.</prompt>' \
          u'<audio src="/recording?lang=tw-GH&amp;val=select_lang" />' \
          u'<choice dtmf="1" next="/api/weather?lang=en-US" />' \
          u'<choice dtmf="2" next="/api/weather?lang=fr-FR" />' \
          u'<choice dtmf="3" next="/api/weather?lang=tw-GH" />' \
          u'</menu>' \
          u'</vxml>'
    return Response(xml, mimetype='text/xml')
    
    
def weather_for_region(lang, code):
    xml = u'<vxml version="2.1" xml:lang="{{language}}">' \
          u'<var name="newWindSpeed" />' \
          u'<var name="newWindDirection" />' \
          u'<var name="newPrecipitation" />' \
          u'<menu id="correct">' \
          u'<property name="inputmodes" value="dtmf"/>' \
          u'<prompt>{{weather_info_initial_prompt}}</prompt>' \
          u'<prompt>{{wind}}</prompt>' \
          u'<prompt>{{precipitations}}</prompt>' \
          u'<prompt>{{outdated}}</prompt>' \
          u'<choice dtmf="1" next="#precipitation" />' \
          u'</menu>' \
          u'<form id="windSpeed">' \
          u'<property name="inputmodes" value="dtmf" />' \
          u'<property name="termchar" value="#" />' \
          u'<field name="windSpeed">' \
          u'<prompt>{{enter_wind_speed}}</prompt>' \
          u'<grammar mode="dtmf" version="1.0" root="data">' \
          u'<rule id="digit" scope="private">' \
          u'<one-of>' \
          u'<item>0</item>' \
          u'<item>1</item>'\
          u'<item>2</item>' \
          u'<item>3</item>' \
          u'<item>4</item>' \
          u'<item>5</item>' \
          u'<item>6</item>' \
          u'<item>7</item>' \
          u'<item>8</item>' \
          u'<item>9</item>' \
          u'</one-of>' \
          u'</rule>' \
          u'<rule id="data" scope="public">' \
          u'<one-of><item repeat="0-"><ruleref uri="#digit" /></item></one-of>' \
          u'</rule>' \
          u'</grammar>' \
          u'<nomatch>' \
          u'<prompt>{{invalid_value}}{{enter_wind_speed}}</prompt>' \
          u'</nomatch>' \
          u'</field>' \
          u'<filled>' \
          u'<assign name="newWindSpeed" expr="windSpeed" />' \
          u'<goto next="#windDirection" />' \
          u'</filled>' \
          u'</form>' \
          u'<form id="windDirection">' \
          u'<property name="inputmodes" value="dtmf" />' \
          u'<property name="termchar" value="#" />' \
          u'<field name="windDirection">' \
          u'<prompt>{{enter_wind_direction}}</prompt>' \
          u'<grammar mode="dtmf" version="1.0" root="data">' \
          u'<rule id="digit" scope="private">' \
          u'<one-of>' \
          u'<item>0</item>' \
          u'<item>1</item>' \
          u'<item>2</item>' \
          u'<item>3</item>' \
          u'<item>4</item>' \
          u'<item>5</item>' \
          u'<item>6</item>' \
          u'<item>7</item>' \
          u'<item>8</item>' \
          u'<item>9</item>' \
          u'</one-of>' \
          u'</rule>' \
          u'<rule id="data" scope="public">' \
          u'<one-of><item repeat="0-"><ruleref uri="#digit" /></item></one-of>' \
          u'</rule>' \
          u'</grammar>' \
          u'<nomatch>' \
          u'<prompt>{{invalid_value}}{{enter_wind_direction}} </prompt>' \
          u'</nomatch>' \
          u'</field>' \
          u'<filled>' \
          u'<assign name="newWindDirection" expr="windDirection" />' \
          u'<goto expr="\'/api/weather/update?lang={{language}}&amp;reg={{region_code}}&amp;ws=\' + newWindSpeed + ' \
          u'\'&amp;wd=\' + newWindDirection + \'&amp;pp=\' + newPrecipitation" />' \
          u'</filled>' \
          u'</form>' \
          u'<form id="precipitation">' \
          u'<property name="inputmodes" value="dtmf" />' \
          u'<property name="termchar" value="#" />' \
          u'<field name="precipitation">' \
          u'<prompt>{{enter_precipitation}}</prompt>' \
          u'<grammar mode="dtmf" version="1.0" root="data">' \
          u'<rule id="digit" scope="private">' \
          u'<one-of>' \
          u'<item>0</item>' \
          u'<item>1</item>' \
          u'<item>2</item>' \
          u'<item>3</item>' \
          u'<item>4</item>' \
          u'<item>5</item>' \
          u'<item>6</item>' \
          u'<item>7</item>' \
          u'<item>8</item>' \
          u'<item>9</item>' \
          u'</one-of>' \
          u'</rule>' \
          u'<rule id="data" scope="public">' \
          u'<one-of><item repeat="0-"><ruleref uri="#digit" /></item></one-of>' \
          u'</rule>' \
          u'</grammar>' \
          u'<nomatch>' \
          u'<prompt>{{invalid_value}}{{enter_precipitations}}</prompt>' \
          u'</nomatch>' \
          u'</field>' \
          u'<filled>' \
          u'<assign name="newPrecipitation" expr="precipitation" />' \
          u'<goto next="#windSpeed" />' \
          u'</filled>' \
          u'</form>' \
          u'</vxml>'
    index = int(code) - 1
    client = MongoClient('localhost', 27017)  # connect to MongoDB running on port 27017
    db = client['ict4d']  # get database 'ict4d'
    weather_collection = db['weather']  # get collection 'weather'
    weather_data = weather_collection.find({})[index]
    localized_strings = db['strings'].find_one({})['strings']
    wind_speed = weather_data['wind']['speed']

    wind_speed = wind_speed_to_range(wind_speed, lang, localized_strings)

    wind_deg = weather_data['wind']['deg']

    wind_direction = degree_to_direction(wind_deg, lang, localized_strings)

    wind = localized_strings['wind'][lang].replace('{{wind_speed}}', str(wind_speed))
    wind = wind.replace('{{wind_direction}}', str(wind_direction))
    weather_info_initial_prompt = localized_strings['weather_info_initial_prompt'][lang].replace("{{region_name}}",
                                                                                                 weather_data['name'])
    pp = ''
    
    if "precipitation" not in weather_data or "value" not in weather_data["precipitation"]:
        pp = localized_strings['no_precipitations'][lang]
    else:
        pp = localized_strings['precipitations'][lang].replace('{{rainfall}}',
                                                               rainfall_to_range(weather_data['precipitation']['value'], lang, localized_strings))
    
    result = inject_language(xml, lang)
    result = result.replace('{{region_code}}', code)
    result = result.replace('{{weather_info_initial_prompt}}', weather_info_initial_prompt)
    result = result.replace('{{wind}}', wind)
    result = result.replace('{{precipitations}}', pp)
    result = result.replace('{{invalid_value}}', localized_strings['invalid_value'][lang])
    result = result.replace('{{outdated}}', localized_strings['outdated'][lang])
    result = result.replace('{{enter_wind_speed}}', localized_strings['enter_wind_speed'][lang])
    result = result.replace('{{enter_wind_direction}}', localized_strings['enter_wind_direction'][lang])
    result = result.replace('{{enter_precipitations}}', localized_strings['enter_precipitations'][lang])
    return Response(result, mimetype='text/xml')


@app.route('/jingle', methods=['GET'])
def get_jingle():
    return send_file('recordings/africanMusic.m4a', mimetype='audio/m4a', as_attachment=False)


@app.route('/recording', methods=['GET'])
def get_recording():
    lang = request.args.get("lang")
    filename = request.args.get("val") + '.wav'
    path = 'recordings/' + lang + '/' + filename
    return send_file(path, mimetype='audio/wav', as_attachment=False)


@app.route('/api/weather', methods=['GET'])
def get_weather():
    lang = request.args.get("lang")

    if lang is None:
        return choose_language()

    code = request.args.get("reg")

    if code is None:
        return choose_region(lang)

    abort(501)
    
    
@app.route('/api/weather/region', methods=['GET'])
def get_region_info():
    lang = request.args.get('lang')
    reg = request.args.get('reg')
    return weather_for_region(lang, reg)


@app.route('/api/weather/update', methods=['GET'])
def update_region():
    lang = request.args.get('lang')
    reg = request.args.get('reg')
    wd = request.args.get('wd')
    ws = request.args.get('ws')
    pp = request.args.get('pp')
    client = MongoClient('localhost', 27017)  # connect to MongoDB running on port 27017
    db = client['ict4d']  # get database 'ict4d'
    weather_collection = db['weather']  # get collection 'weather
    weather_data = weather_collection.find({})[int(reg) - 1]
    weather_collection.update({"_id": weather_data["_id"]}, {"$set": {
        'wind.speed': int(ws),
        'wind.deg': int(wd),
        'precipitation.value': int(pp)
    }})  # update weather info
    localized_strings = db['strings'].find_one({})['strings']
    result = u'<vxml version="2.1"><prompt>{{thank_you}}</prompt></vxml>'
    result = result.replace('{{thank_you}}', localized_strings['thank_you'][lang])
    return Response(result, mimetype='text/xml')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
