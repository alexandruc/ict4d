# encoding: utf-8
import json
import os
import requests
import shutil
import sys
from pymongo import MongoClient

api_key = ''
req_url = 'http://api.openweathermap.org/data/2.5/weather?q={0}&APPID={1}'
ttslab_package_path = '~/tts/ttslab/ttslab'


def region_code(index):
    if index < 10:
        return "0" + str(index)
    return str(index)


def synthesize(wordus, phrase, filename):
    utt = wordus.synthesize(phrase, "text-to-wave")
    utt["waveform"].write(filename)
    shutil.move(filename, 'recordings/tw-GH/' + filename)


def synthesize_numbers(voice):
    synthesize(voice, u'Hwee Baako', '01.wav')
    synthesize(voice, u'Hwee Mmienu', '02.wav')
    synthesize(voice, u'Hwee Mmeɛnsa', '03.wav')
    synthesize(voice, u'Hwee ɛnan', '04.wav')
    synthesize(voice, u'Hwee ɛnum', '05.wav')
    synthesize(voice, u'Hwee Nsia', '06.wav')
    synthesize(voice, u'Hwee Nson', '07.wav')
    synthesize(voice, u'Hwee Nwɔtwe', '08.wav')
    synthesize(voice, u'Hwee Nkron', '09.wav')
    synthesize(voice, u'Baako Hwee', '10.wav')
    synthesize(voice, u'Baako Baako', '11.wav')
    synthesize(voice, u'Baako Mmienu', '12.wav')
    synthesize(voice, u'Baako Mmeɛnsa', '13.wav')
    synthesize(voice, u'Hwee', '0.wav')
    synthesize(voice, u'Baako', '1.wav')
    synthesize(voice, u'Mmienu', '2.wav')
    synthesize(voice, u'Mmeɛnsa', '3.wav')
    synthesize(voice, u'ɛnan', '4.wav')
    synthesize(voice, u'ɛnum', '5.wav')
    synthesize(voice, u'Nsia', '6.wav')
    synthesize(voice, u'Nson', '7.wav')
    synthesize(voice, u'Nwɔtwe', '8.wav')
    synthesize(voice, u'Nkron', '9.wav')


def generate_autio_files():
    global ttslab_package_path

    if not os.path.isdir("./recordings"):
        os.makedirs("recordings")

    if os.path.isdir("./recordings/tw-GH"):
        shutil.rmtree("recordings/tw-GH")

    os.makedirs("recordings/tw-GH")
    sys.path.insert(0, ttslab_package_path)

    import ttslab

    voice = ttslab.fromfile("wordus.voice.pickle")
    synthesize(voice, u'Sɛ wo pɛ twi aa mea Mmeɛnsa', 'select_lang.wav')
    synthesize_numbers(voice)
    synthesize(voice, u'Mea', 'press.wav')
    synthesize(voice, u'na hunu ewiem nsakrayɛ aa ɛwɔ', 'for_region.wav')
    synthesize(voice, u'ewiem nsakrayɛ aa ɛwɔ', 'weather_for.wav')
    synthesize(voice, u'nsuo', 'rain.wav')
    synthesize(voice, u'nsuo rentɔ', 'no_rain.wav')
    synthesize(voice, u'Nyɛ ne Kwan so nono. San yɛ no fofroɔ', 'invalid_entry.wav')
    synthesize(voice, u'Medase', 'thank_you.wav')
    synthesize(voice, u'nsuo: ɛhum', "storm.wav")
    synthesize(voice, u'nsuo: ɛhai nsuo', "light_rain.wav")
    synthesize(voice, u'nsuo: ɛyaara nsuo', "average_rain.wav")
    synthesize(voice, u'nsuo: dene nsuo', "heavy_rain.wav")
    synthesize(voice, u'nsuo: dene paa nsuo', "very_heavy_rain.wav")


def init():
    client = MongoClient('localhost', 27017)  # connect to MongoDB running on port 27017
    db = client['ict4d']  # get database 'ict4d'
    weather_collection = db['weather']  # get collection 'weather'
    regions_collection = db['regions']
    strings_collection = db['strings']
    weather_collection.delete_many({})  # clear all documents
    regions_collection.delete_many({})
    strings_collection.delete_many({})

    with open('regions.json') as regions_file:
        data = json.load(regions_file)
        regions_collection.insert_one(data)

    with open('weather_strings.json') as strings_file:
        data = json.load(strings_file)
        strings_collection.insert_one(data)
        districts = regions_collection.find_one({})

        for district in districts['regions']:
            for region in districts['regions'][district]:
                url = req_url.format(region + ",BF", api_key)
                response = requests.get(url)  # query open weather data API

                if response.ok:  # if query succeeded
                    content = json.loads(response.content)
                    weather_collection.insert_one(content)  # update weather info

if __name__ == '__main__':
    init()
    generate_autio_files()
